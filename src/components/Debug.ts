import { Application } from 'pixi.js'

export class Debug {

    static renderer?: CanvasRenderingContext2D

    static instantiate(renderer: CanvasRenderingContext2D, app?: Application) {
        this.renderer = renderer;
        renderer.globalAlpha = 1;
        renderer.strokeStyle = 'rgba(255, 0, 0, 1)';
        renderer.fillStyle = 'rgba(255, 0, 0, 1)';
    }

    static drawLine(startX: number, startY: number, endX: number, endY: number, style?: string | CanvasGradient | CanvasPattern) {
        const { renderer } = this;
        if (!renderer) { return }
        const strokeStyle = renderer.strokeStyle;
        if (style) {
            renderer.strokeStyle = style;
        }
        renderer.beginPath()
        renderer.moveTo(startX, startY)
        renderer.lineTo(endX, endY)
        renderer.stroke()
        renderer.strokeStyle = strokeStyle;
    }

    static drawPoint(x: number, y: number, radius: number = 1, style?: string | CanvasGradient | CanvasPattern) {
        const { renderer } = this;
        if (!renderer) { return }
        const fillStyle = renderer.fillStyle;
        if (style) {
            renderer.fillStyle = style;
        }
        if (radius > 1) {
            renderer.beginPath()
            renderer.arc(x, y, radius, 0, 2*Math.PI, false)
            renderer.fill()
        } else {
            renderer.fillRect(x, y, 1, 1)
        }
        renderer.fillStyle = fillStyle;
    }
}