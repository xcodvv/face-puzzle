import { Application, Rectangle, Sprite } from 'pixi.js'
import { UserInput, UserInputEventType, UserScaleEvent, UserTouch, UserTouchEventType } from './UserInput'
import { TouchableSprite } from './TouchableSprite'
import { Debug } from '../components/Debug'

export { TouchableSprite } from './TouchableSprite'

export interface TouchableSpriteRaysCasts {
    target: TouchableSprite
    raysCount: number
}

/**
 * The object holds TouchableSprite`s to handle touch events
 * @class
 */
export class TouchableObjects {

    static readonly RayDistance = 25
    static readonly TotalRays = 20
    static userInput = UserInput.shared
    static touches = new Array<UserTouch>()
    static sprites = new Array<TouchableSprite>()
    static target?: TouchableSprite
    static targets = new Map<number, TouchableSprite>()
    static touchesZones = new Array<Rectangle>()
    private static sortDirty = false
    private static topZIndex = 0
    private static rays: Array<[number, number]> = [ ]

    /**
     * You must call this function once to start handling a touch events for a TouchableSprite`s
     * @static
     * @param app PIXI.js Application instance
     */
    static instantiate(app?: Application) {
        const rayStep = (Math.PI*2)/this.TotalRays;
        for(let i=0; i<this.TotalRays; ++i) {
            const rx = Math.cos(rayStep*i);
            const ry = Math.sin(rayStep*i);
            this.rays.push([rx, ry])
        }

        this.userInput.enable()

        this.userInput.on(UserInputEventType.Touch, ev=>{
            const touch = ev.detail as UserTouch;
            this.touches.push(touch)
            touch.once(UserTouchEventType.End, ()=>this.touches.splice(this.touches.findIndex(t=>t===ev.detail), 1))
        })

        this.userInput.on(UserInputEventType.Scale, ev=>{
            if (!this.userInput.pointer) return
            const scaleDelta = (ev as UserScaleEvent).detail;
            const target = this.targets.get(this.userInput.pointer.id)
            if (target) {
                target.setScale(Math.max(target.scaling.minScale, target.scale.x-scaleDelta))
            }
        })

        this.userInput.on(UserInputEventType.Rotate, ev=>{
            if (!this.userInput.pointer) return
            const rotateRate = (ev as UserScaleEvent).detail;
            const target = this.targets.get(this.userInput.pointer.id)
            if (target) {
                target.rotation += Math.sign(rotateRate)*Math.min(Math.PI/10, Math.abs(rotateRate)*Math.PI)
            }
        })

        app?.ticker.add(()=>this.ticker())
    }

    /**
     * Adds sprite to the sprites array to handle touch events for this sprite.
     * You don`t need to call this function manually, it calls from TouchableSprite constructor.
     * @static
     * @param sprite TouchableSprite class instance that needs to be a touchable
     */
    static addSprite(sprite: TouchableSprite) {
        this.sprites.push(sprite)
        this.setSortDirty()
    }

    /**
     * Removes sprite from sprites array that handle touch events
     * @static
     * @param sprite TouchableSprite class instance that needs to remove
     */
    static removeSprite(sprite: Sprite) {
        const n = this.sprites.findIndex(s=>s===sprite)
        if (n<0) { return }
        this.sprites.splice(n, 1)
    }

    /**
     * Makes resort at next render tick (before render itself).
     * You don`t need to call this function manually, it calls from TouchableSprite when zIndex property changed.
     * @static
     */
    static setSortDirty() {
        this.sortDirty = true;
    }

    /**
     * Reset for all sprites active drag and scale gestures
     * @static
     */
    static reset() {
        this.sprites.forEach(sprite=>{
            if (sprite.drag.identifier > 0) {
                sprite.drag.identifier = 0;
                sprite.scaling.identifier = 0;
            }
        })
    }

    /**
     * This function will be called for each application tick (render tick) if PIXI.js Application instance was given at instatiate call.
     * Instead of using application ticker you can call this function manually from requestAnimationFrame callback. 
     * @static
     */
    static ticker() {
        if (this.sortDirty) {
            this.sortDirty = false;
            if (this.sprites.length > 0) {
                this.sprites.sort((a,b)=>b.zIndex-a.zIndex)
                this.topZIndex = this.sprites[0].zIndex;
            }
        }

        if (this.touchesZones.length > 0) {
            this.touchesZones.forEach(zone=>{
                const touches = this.touches.filter(touch=>zone.contains(touch.startEvent.clientX, touch.startEvent.clientY))
                if (touches.length > 0) {
                    const targets = new Map<number, TouchableSprite>()
                    const check = new Map<number, TouchableSprite>()
                    for(const [id, target] of this.targets.entries()) {
                        if (touches.find(t=>t.id===id)) {
                            targets.set(id, target)
                            check.set(id, target)
                        }
                    }
                    this.hitTouches(touches, targets)
                    for(const [id] of check.entries()) {
                        if (!targets.has(id)) {
                            this.targets.delete(id)
                        }
                    }
                    for(const [id, target] of targets.entries()) {
                        this.targets.set(id, target)
                    }
                }
            })
        } else {
            this.hitTouches(this.touches, this.targets)
        }

        this.sprites.forEach(sprite=>{
            if (sprite.drag.identifier!==0) {
                this.updateSprite(sprite)
            }
        })
    }

    static hitTouches(touches: Array<UserTouch>, targets: Map<number, TouchableSprite>) {
        if (touches.length===2) {
            if (targets.size === 0) {
                // try to bind the center of two touches with a sprite
                const [ firstTouch, secondTouch ] = touches;
                const [ px, py ] = firstTouch.position;
                const [ sx, sy ] = secondTouch.position;
                const tv = [ sx-px, sy-py ];
                const [ cx, cy ] = [ px+tv[0]/2, py+tv[1]/2 ];
                Debug.drawPoint(cx, cy, 10, 'rgba(0, 0, 255, 0.35)')
                const target = this.hitRaysFirst(cx, cy, 10)
                if (target) {
                    targets.set(firstTouch.id, target)
                    targets.set(secondTouch.id, target)
                    target.zIndex = this.topZIndex+=1;
                    target.drag.identifier = firstTouch.id;
                    target.drag.isStarted = false
                    target.scaling.identifier = secondTouch.id;
                    target.scaling.isStarted = false
                    touches.forEach(touch=>{
                        touch.once(UserTouchEventType.End, ()=>{
                            if (target.drag.identifier === touch.id) {
                                target.drag.identifier = 0
                                target.drag.isStarted = false
                            }
                            if (target.scaling.identifier === touch.id) {
                                target.scaling.identifier = 0
                                target.scaling.isStarted = false
                            }
                            targets.delete(touch.id)
                            this.targets.delete(touch.id)
                        })
                    })
                }
            }
        }

        touches.forEach(touch=>{
            const [ px, py ] = touch.position;
            let maxRadius = touch.endEvent instanceof Touch ? Math.max(0, touch.endEvent.radiusX-10, touch.endEvent.radiusY-10) : 0;
            if (maxRadius <= 0 && this.userInput.pointer!==touch) maxRadius = TouchableObjects.RayDistance;
            if (maxRadius > 0) Debug.drawPoint(px, py, maxRadius, 'rgba(255, 255, 255, 0.35)')

            if (!targets.has(touch.id)) {
                const sprite = this.hitTouch(touch, maxRadius)
                if (!sprite) return

                targets.set(touch.id, sprite)

                touch.once(UserTouchEventType.End, ()=>{
                    const target = targets.get(touch.id)
                    if (target) {
                        if (sprite.drag.identifier === touch.id) {
                            target.drag.identifier = 0
                            target.drag.isStarted = false
                        }
                        if (sprite.scaling.identifier === touch.id) {
                            target.scaling.identifier = 0
                            target.scaling.isStarted = false
                        }
                    }
                    targets.delete(touch.id)
                    this.targets.delete(touch.id)
                })
            }
        })

        if (touches.length===2) {
            const target = targets.values().next().value as TouchableSprite;
            if (target) {
                // bind second touch if we got only one target with two touches
                touches.some(touch=>{
                    if (targets.has(touch.id)) return false
                    targets.set(touch.id, target)
                    target.scaling.identifier = touch.id;
                    target.scaling.isStarted = false;
                    target.zIndex = this.topZIndex+=1;
                    touch.once(UserTouchEventType.End, ()=>{
                        if (target.drag.identifier === touch.id) {
                            target.drag.identifier = 0
                            target.drag.isStarted = false
                        }
                        if (target.scaling.identifier === touch.id) {
                            target.scaling.identifier = 0
                            target.scaling.isStarted = false
                        }
                        targets.delete(touch.id)
                        this.targets.delete(touch.id)
                    })
                    return true
                })
            }
        }
    }

    /**
     * Handles a UserTouch class instance to find which TouchableSprite touched
     * @static
     * @param touch UserTouch class instance
     * @returns instance of TouchableSprite or undefined if no sprite touched
     */
    static hitTouch(touch: UserTouch, maxRadius: number = TouchableObjects.RayDistance): TouchableSprite | undefined {
        const [ px, py ] = touch.path[0];
        // const raysCasts = this.hitRaysCount(px, py, maxRadius)
        // const targets = raysCasts.sort((a,b)=>b.raysCount-a.raysCount).map(({ target })=>target)
        const targets = this.hitRaysAll(px, py, maxRadius)
        const target = targets.find(t=>t.drag.identifier!==0 && t.scaling.identifier===0) ?? targets[0];
        if (target) {
            if (target.drag.identifier !== 0) {
                if (target.scaling.identifier===0) {
                    target.scaling.identifier = touch.id;
                    target.scaling.isStarted = false;
                    target.zIndex = this.topZIndex+=1;
                } else {
                    return
                }
            } else if (target.drag.identifier!==touch.id) {
                target.drag.identifier = touch.id;
                target.drag.isStarted = false;
                target.zIndex = this.topZIndex+=1;
            }
        }
        return target
    }

    /**
     * Finds first TouchableSprite that hits by point in global space
     * @param x X in global space
     * @param y Y in global space
     * @returns sprite if hits by point
     */
    static hitPoint(x: number, y: number): TouchableSprite | undefined {
        let target: TouchableSprite | undefined
        this.sprites.some(sprite=>{
            if (!sprite.visible) return false
            if (sprite.hitTestPoint(x, y)) target = sprite
            return !!target
        })
        return target
    }

    /**
     * Finds first TouchableSprite that hits by one of the rays with specified distance
     * @param originX ray origin X in global space
     * @param originY ray origin Y in global space
     * @param rayDistance ray distance to check
     * @returns sprite if hits by one of the rays
     */
    static hitRaysWithDistance(originX: number, originY: number, rayDistance: number): TouchableSprite | undefined {
        if (rayDistance <= 0) return this.hitPoint(originX, originY)
        let target: TouchableSprite | undefined
        this.sprites.some(sprite=>{
            if (!sprite.visible) return false
            const hitRays = this.rays.some(([rx,ry])=>sprite.hitTestPoint(originX+rx*rayDistance, originY+ry*rayDistance))
            if (hitRays) target = sprite
            return hitRays
        })
        return target
    }

    /**
     * Finds all TouchableSprite that hits by the rays with specified distance and counts hitted rays
     * @param originX ray origin X in global space
     * @param originY ray origin Y in global space
     * @param rayDistance ray distance to check
     * @returns array of the sprites with the rays hitting counts
     */
     static hitRaysCountWithDistance(originX: number, originY: number, rayDistance: number): TouchableSpriteRaysCasts[] {
        const raysCasts = new Array<TouchableSpriteRaysCasts>()
        if (rayDistance <= 0) {
            const target = this.hitPoint(originX, originY)
            if (target) raysCasts.push({ target, raysCount:1 })
            return raysCasts
        }
        this.rays.forEach(([rx,ry])=>{
            const [ rayX, rayY ] = [ originX+rx*rayDistance, originY+ry*rayDistance ];
            this.sprites.forEach(sprite=>{
                if (!sprite.hitTestPoint(rayX, rayY)) return;
                const hitRaysCasts = raysCasts.find(({target})=>target===sprite)
                if (hitRaysCasts) {
                    hitRaysCasts.raysCount += 1;
                } else {
                    raysCasts.push({ target:sprite, raysCount:1 })
                }
            })
        })
        return raysCasts
    }

    /**
     * Finds all TouchableSprite that hits by the rays with specified max distance and counts hitted rays
     * @param originX ray origin X in global space
     * @param originY ray origin Y in global space
     * @param maxRayDistance max ray distance to check
     * @returns array of the sprites with the rays hitting counts
     */
    static hitRaysCount(originX: number, originY: number, maxRayDistance: number = TouchableObjects.RayDistance): TouchableSpriteRaysCasts[] {
        const raysCasts = new Array<TouchableSpriteRaysCasts>()
        for(let dist=0; dist<maxRayDistance; ++dist) {
            this.hitRaysCountWithDistance(originX, originY, dist).forEach(({ target:sprite, raysCount })=>{
                const hitsRaysCasts = raysCasts.find(({ target })=>target===sprite)
                if (hitsRaysCasts) {
                    hitsRaysCasts.raysCount += raysCount;
                } else {
                    raysCasts.push({ target:sprite, raysCount })
                }
            })
        }
        return raysCasts
    }

    /**
     * Finds first TouchableSprite that hits by one of the rays with specified max distance
     * @param originX ray origin X in global space
     * @param originY ray origin Y in global space
     * @param maxRayDistance max ray distance to check (from 0 to max distance)
     * @returns sprite if hits by one of the rays
     */
    static hitRaysFirst(originX: number, originY: number, maxRayDistance: number = TouchableObjects.RayDistance): TouchableSprite | undefined {
        for(let dist=0; dist<maxRayDistance; ++dist) {
            const sprite = this.hitRaysWithDistance(originX, originY, dist)
            if (sprite) return sprite
        }
    }

    /**
     * Finds all TouchableSprite that hits by the rays with specified max distance
     * @param originX ray origin X in global space
     * @param originY ray origin Y in global space
     * @param maxRayDistance max ray distance to check (from 0 to max distance)
     * @returns array of the sprites that hits by the rays
     */
    static hitRaysAll(originX: number, originY: number, maxRayDistance: number = TouchableObjects.RayDistance): TouchableSprite[] {
        const sprites = new Array<TouchableSprite>()
        if (maxRayDistance <= 0) {
            const sprite = this.hitPoint(originX, originY)
            if (sprite) sprites.push(sprite)
        } else {
            for(let dist=0; dist<maxRayDistance; ++dist) {
                const sprite = this.hitRaysWithDistance(originX, originY, dist)
                if (sprite) sprites.push(sprite)
            }
        }
        return sprites
    }

    /**
     * Updates sprite with associated UserTouch`s
     * @static
     * @param sprite instance of TouchableSprite class that needs to update
     */
    static updateSprite(sprite: TouchableSprite) {
        const firstTouch = this.touches.find(t=>t.id===sprite.drag.identifier)
        if (!firstTouch) return

        const [ px, py ] = firstTouch.position;
        const dragPosition = sprite.parent.toLocal({x:px, y:py}, undefined, undefined, true)
        if (!sprite.drag.isStarted) {
            sprite.drag.isStarted = true;
            const offset = sprite.toLocal({x:px, y:py}, undefined, undefined, true)
            sprite.pivot.set(offset.x, offset.y)
            sprite.drag.pivotPoint = [offset.x, offset.y];
        }
        if (sprite.isDragable) {
            sprite.x = dragPosition.x;
            sprite.y = dragPosition.y;
        }
        
        const secondTouch = this.touches.find(t=>t.id===sprite.scaling.identifier)
        if (!secondTouch) return

        const [ sx, sy ] = secondTouch.position;
        
        const tv = [ sx-px, sy-py ];
        const touchesDistance = Math.sqrt(tv[0]*tv[0]+tv[1]*tv[1])
        const touchesRotation = Math.atan2(tv[1], tv[0])
        const [ cx, cy ] = [ px+tv[0]/2, py+tv[1]/2 ];
        Debug.drawPoint(cx, cy, 10, 'rgba(0, 0, 255, 0.35)')

        const secondPosition = sprite.parent.toLocal({x:sx, y:sy}, undefined, undefined, true)
        const touchesVector = [ secondPosition.x-dragPosition.x, secondPosition.y-dragPosition.y ];
        const [ touchesCenterX, touchesCenterY ] = [ dragPosition.x+touchesVector[0]/2, dragPosition.y+touchesVector[1]/2 ];

        if (!sprite.scaling.isStarted) {
            sprite.scaling.isStarted = true;
            const offset = sprite.toLocal({x:cx, y:cy}, undefined, undefined, true)
            sprite.pivot.set(offset.x, offset.y)
            sprite.scaling.fromDistance = touchesDistance;
            sprite.scaling.fromScale = sprite.scale.x;
            sprite.scaling.fromRotation = sprite.rotation-touchesRotation;
            secondTouch.once(UserTouchEventType.End, ()=>{
                if (sprite.drag.identifier === firstTouch.id) {
                    sprite.pivot.set(sprite.drag.pivotPoint[0], sprite.drag.pivotPoint[1])
                    const firstPosition = sprite.parent.toLocal({x:firstTouch.position[0], y:firstTouch.position[1]}, undefined, undefined, true)
                    sprite.x = firstPosition.x;
                    sprite.y = firstPosition.y;
                }
            })
            firstTouch.once(UserTouchEventType.End, ()=>{
                if (sprite.scaling.identifier === secondTouch.id) {
                    sprite.scaling.identifier = 0
                    sprite.scaling.isStarted = false
                    sprite.drag.identifier = secondTouch.id
                    sprite.drag.isStarted = false
                }
            })
        }
        if (sprite.isDragable) {
            sprite.x = touchesCenterX;
            sprite.y = touchesCenterY;
        }
        
        if (sprite.isScaleable) {
            const scaleValue = touchesDistance/sprite.scaling.fromDistance;
            sprite.scaling.targetScale = Math.max(sprite.scaling.minScale, sprite.scaling.fromScale*scaleValue)
        }
        
        if (sprite.isRotateable) {
            sprite.rotation = sprite.scaling.fromRotation + touchesRotation;
        }

        const k = 0.3;
        const scaleValue = k*sprite.scaling.targetScale+(1-k)*sprite.scale.x;
        sprite.scale.set(scaleValue, scaleValue)
    }
}