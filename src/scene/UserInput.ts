import { defer } from '../utils'

export class UserInput {

    static shared = new UserInput(window)

    delegate = document.createDocumentFragment()
    target: any
    pointer: UserTouch | null = null
    touches: Array<UserTouch> = [ ]
    firstInput: TouchEvent | MouseEvent | null = null
    isEnabled: boolean = false
    private disabling = defer<UserInput>()

    constructor(target: any) {
        this.target = target;
    }

    off(type: UserInputEventType, listener: any, options?: boolean | AddEventListenerOptions | undefined) {
        return this.delegate.removeEventListener.apply(this.delegate, arguments as any)
    }

    on(type: UserInputEventType, listener: (e: UserTouchEvent | UserScaleEvent | UserRotateEvent)=>void, options?: boolean | AddEventListenerOptions | undefined) {
        return this.delegate.addEventListener.apply(this.delegate, arguments as any)
    }

    once(type: UserInputEventType, listener: (e: UserTouchEvent)=>void, options?: AddEventListenerOptions | undefined) {
        if (options) {
            options = Object.assign(options, { once: true })
        } else {
            options = { once: true }
        }
        return this.delegate?.addEventListener(type, listener as any, options)
    }

    emit(type: UserInputEventType, detail: any) {
        return this.delegate.dispatchEvent(new CustomEvent(type, { detail }))
    }

    disable() {
        if (!this.isEnabled) { return }
        this.isEnabled = false;
        this.disabling.fulfill(this)
        this.disabling = defer<UserInput>()
    }

    enable() {
        const { target } = this;
        if (this.isEnabled) { return }
        this.isEnabled = true;

        const pointerStartListen = this.pointerStartListen.bind(this)
        const pointerMoveListen = this.pointerMoveListen.bind(this)
        const pointerEndListen = this.pointerEndListen.bind(this)
        const pointerWheelListen = this.pointerWheelListen.bind(this)
        target.addEventListener('mousedown', pointerStartListen)
        target.addEventListener('mousemove', pointerMoveListen)
        target.addEventListener('mouseup', pointerEndListen)
        target.addEventListener('mousewheel', pointerWheelListen)

        const touchStartListen = this.touchStartListen.bind(this)
        const touchMoveListen = this.touchMoveListen.bind(this)
        const touchEndListen = this.touchEndListen.bind(this)
        target.addEventListener('touchstart', touchStartListen)
        target.addEventListener('touchmove', touchMoveListen)
        target.addEventListener('touchend', touchEndListen)
        target.addEventListener('touchcancel', touchEndListen)

        this.disabling.then(()=>{
            target.removeEventListener('mousedown', pointerStartListen)
            target.removeEventListener('mousemove', pointerMoveListen)
            target.removeEventListener('mouseup', pointerEndListen)
            target.removeEventListener('mousewheel', pointerWheelListen)
            target.removeEventListener('touchstart', touchStartListen)
            target.removeEventListener('touchmove', touchMoveListen)
            target.removeEventListener('touchend', touchEndListen)
            target.removeEventListener('touchcancel', touchEndListen)
        })
    }

    pointerStartListen(event: MouseEvent) {
        event.preventDefault()
        event.stopPropagation()
        this.pointer = new UserTouch(event)
        this.emit(UserInputEventType.Touch, this.pointer)
    }

    pointerMoveListen(event: MouseEvent) {
        if (!this.pointer) { return }
        event.preventDefault()
        event.stopPropagation()
        this.pointer.deltaTime = event.timeStamp - this.pointer.timeStamp;
        this.pointer.move(event)
    }

    pointerEndListen(event: MouseEvent) {
        if (!this.pointer) { return }
        event.preventDefault()
        event.stopPropagation()
        this.pointer.deltaTime = event.timeStamp - this.pointer.timeStamp;
        this.pointer.end(event)
    }

    pointerWheelListen(event: WheelEvent) {
        if (!this.pointer) { return }
        const startEvent = this.pointer.startEvent as MouseEvent;
        if (startEvent.button === 0) {
            this.emit(UserInputEventType.Scale, event.deltaY*-0.01)
        } else if (startEvent.button === 2) {
            this.emit(UserInputEventType.Rotate, event.deltaY*-0.01)
        }
    }

    touchStartListen(event: TouchEvent) {
        const touches = Array.prototype.map.call(event.changedTouches, (t: Touch)=>new UserTouch(t)) as Array<UserTouch>
        this.touches = this.touches.concat(touches as any)
        touches.forEach(u=>{
            u.timeStamp = event.timeStamp;
            this.emit(UserInputEventType.Touch, u)
        })
    }

    touchMoveListen(event: TouchEvent) {
        Array.prototype.map.call(event.changedTouches, (t: Touch)=>{
            const u = this.touches.find(u=>u.id===t.identifier)
            if (u) {
                u.deltaTime = event.timeStamp - u.timeStamp;
                u.move(t)
            }
        })
    }

    touchEndListen(event: TouchEvent) {
        Array.prototype.map.call(event.changedTouches, (t: Touch)=>{
            let n = this.touches.findIndex(u=>u.id==t.identifier)
            if (n >= 0) {
                const [ u ] = this.touches.splice(n, 1)
                u.deltaTime = event.timeStamp - u.timeStamp;
                u.end(t)
            }
        })
    }

}

export interface UserTouchEvent extends CustomEvent {
    detail: UserTouch
}

export interface UserScaleEvent extends CustomEvent {
    detail: number
}

export interface UserRotateEvent extends CustomEvent {
    detail: number
}

export type UserInputEvent = UserTouchEvent | UserScaleEvent | UserRotateEvent

export enum UserInputEventType {
    Touch = 'touch',
    Scale = 'scale',
    Rotate = 'rotate',
}

export enum UserTouchEventType {
    Move = 'move',
    End = 'end',
}

export class UserTouch {

    id: number
    startEvent: Touch | MouseEvent
    endEvent: Touch | MouseEvent | null | undefined
    path: Array<[number, number]> = [ ]
    position: [number, number] = [0, 0]
    direction: [number, number] = [0, 0]
    length: number = 0
    angle: number = 0
    timeStamp: number = 0
    deltaTime: number = 0
    delegate? = document.createDocumentFragment()

    constructor(touch: Touch | MouseEvent) {
        this.startEvent = touch;
        this.id = touch instanceof MouseEvent ? 1 : touch.identifier;
        this.position = [touch.clientX, touch.clientY];
        this.path.push(this.position)
    }

    move(touch: Touch | MouseEvent) {
        this.endEvent = touch;
        const v = [
            touch.clientX-this.startEvent.clientX,
            touch.clientY-this.startEvent.clientY,
        ];
        this.angle = Math.atan2(-v[1], v[0])*(180/Math.PI)
        this.length = Math.sqrt(v[0]*v[0]+v[1]*v[1])
        this.direction = [v[0], -v[1]];
        this.position = [touch.clientX, touch.clientY];
        this.path.push(this.position)
        this.emit(UserTouchEventType.Move, this)
    }

    end(touch: Touch | MouseEvent) {
        this.endEvent = touch;
        const v = [
            touch.clientX-this.startEvent.clientX,
            touch.clientY-this.startEvent.clientY,
        ];
        this.angle = Math.atan2(-v[1], v[0])*(180/Math.PI)
        this.length = Math.sqrt(v[0]*v[0]+v[1]*v[1])
        this.direction = [v[0], -v[1]];
        this.position = [touch.clientX, touch.clientY];
        this.path.push(this.position)
        this.emit(UserTouchEventType.End, this)
        this.delegate = undefined
    }

    emit(type: UserTouchEventType, detail: any) {
        return this.delegate?.dispatchEvent(new CustomEvent(type, { detail }))
    }

    off(type: UserTouchEventType, listener: any, options?: boolean | AddEventListenerOptions | undefined) {
        return this.delegate?.removeEventListener.apply(this.delegate, arguments as any)
    }

    on(type: UserTouchEventType, listener: (e: UserInputEvent)=>void, options?: boolean | AddEventListenerOptions | undefined) {
        return this.delegate?.addEventListener.apply(this.delegate, arguments as any)
    }

    once(type: UserTouchEventType, listener: (e: UserInputEvent)=>void, options?: AddEventListenerOptions | undefined) {
        if (options) {
            options = Object.assign(options, { once: true })
        } else {
            options = { once: true }
        }
        return this.delegate?.addEventListener(type, listener as any, options)
    }

}