import { Application, BaseTexture, Container, ILoaderResource, Resource, Sprite, Texture } from 'pixi.js'
import { TouchableObjects, TouchableSprite } from './TouchableObjects'
import { defer } from '../utils'

/**
 * The Puzzle object is the scene controller that can load texture atlas and create a TouchableSprite`s.
 * Call ```release``` function to destroy all used resources.
 * You can reuse the Puzzle object to create another scene.
 * @class
 */
export class Puzzle {

    app: Application
    private sprites: Array<Sprite> = [ ]

    /**
     * @param app PIXI.js Application class instance
     */
    constructor(app: Application) {
        this.app = app;
    }

    /**
     * Removes a texture cache and destroys a texture
     * @param tex PIXI.js Texture class instance that needs to be destroyed
     */
    destroyTexture(tex: Texture | undefined) {
        if (!tex) { return }
        Texture.removeFromCache(tex)
        BaseTexture.removeFromCache(tex.baseTexture)
        tex.destroy()
    }
    
    /**
     * Removes PIXI.js loader resource and all associated textures
     * @param res PIXI.js loader resource that needs to be destroyed
     */
    destroyResource(res: ILoaderResource) {
        this.destroyTexture(res.texture)
        if (res.textures) {
            for(let name in res.textures) {
                this.destroyTexture(res.textures[name])
            }
        }
        res.children.forEach(r=>{
            this.destroyResource(r)
        })
        delete this.app.loader.resources[res.name];
    }

    /**
     * Cleans out all scene resources and reset PIXI.js loaders
     */
    release() {
        const { sprites } = this;
        this.sprites.forEach(sprite=>{
            sprite.removeChildren()
            sprite.parent.removeChild(sprite)
            TouchableObjects.removeSprite(sprite)
        })
    }

    /**
     * Awakes a puzzle scene, adds puzzle TouchableSprite`s into the PIXI.js Application stage or into container if specified
     * @param container optional PIXI.js Container, if not specified will be used application stage
     */
    async awake(textures: Array<Texture<Resource>>, radius: number, scaleRate: number, container?: Container) {
        const { app } = this;
        if (!container) {
            container = app.stage;
        }
        if (!container.sortableChildren) {
            container.sortableChildren = true;
        }

        let maxSize = [0, 0];
        textures.forEach(tex=>{
            if (tex.width > maxSize[0]) maxSize[0] = tex.width;
            if (tex.height> maxSize[1]) maxSize[1] = tex.height;
        })
        const maxSpace = maxSize[0]*maxSize[1];

        const step = (2*Math.PI)/textures.length;
        let rotation = 0;
        textures
            .sort(()=>0.5-Math.random())
            .forEach(texture=>{
                const sprite = new TouchableSprite(texture)
                this.sprites.push(sprite)
                sprite.enableAlphaTest(app)
                sprite.anchor.set(0.5, 0.5)
                sprite.rotation = Math.random()*(Math.PI*2)
                sprite.x = Math.cos(rotation)*radius;
                sprite.y = Math.sin(rotation)*radius;
                container?.addChild(sprite)

                const space = sprite.width*sprite.height;
                sprite.setScale((1-space/maxSpace)*scaleRate)
                sprite.scaling.minScale = 0.2;

                rotation += step;
            })
    }

}