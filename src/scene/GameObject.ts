import { Container, Resource, Sprite, Texture } from 'pixi.js'

export class GameObject extends Container {

    private spriteComponentSeq = 0
    sprites: { [id: string]: Sprite } = { }

    makeSprite(texture?: Texture<Resource>): Sprite {
        const name = `Sprite${++this.spriteComponentSeq}`;
        const sprite = this.sprites[name] = new Sprite(texture)
        this.addChild(sprite)
        return sprite
    }

    addSprite(name: string, texture?: Texture<Resource>): Sprite {
        const sprite = this.sprites[name] = new Sprite(texture)
        this.addChild(sprite)
        return sprite
    }
}