import { Application, Resource, Sprite, Texture } from 'pixi.js'
import { TouchableObjects } from './TouchableObjects'

/**
 * The object extends PIXI.js Sprite and makes it available for the touching
 * @class
 */
export class TouchableSprite extends Sprite {
    
    static zIndexSeq = 0;
    isDragable: boolean = true
    drag: DragTouch = new DragTouch()
    scaling: ScaleTouch = new ScaleTouch()
    isScaleable: boolean = true
    isRotateable: boolean = true
    isCatchable: boolean = false
    pixelData?: Uint8Array
    enabled: boolean = true

    /**
     * @param texture the texture for this sprite
     */
    constructor(texture?: Texture<Resource>) {
        super(texture)
        this.zIndex = TouchableSprite.zIndexSeq+=1;
        this.scaling.targetScale = this.scale.x;
        TouchableObjects.addSprite(this)
    }

    /**
     * Enable interactions only with visible pixels of sprite
     * @param app the PIXI.js application instance
     */
    enableAlphaTest(app: Application) {
        if (this.texture) {
            this.pixelData = app.renderer.plugins.extract.pixels(this)
        }
    }

    /**
     * Test if point hits the sprite
     * @param x point X position
     * @param y point Y position
     * @returns ```true``` if point hits the sprite
     */
    hitTestPoint(x: number, y: number): boolean {
        if (!this.visible) { return false }
        const ox = this.width*this.anchor.x;
        const oy = this.height*this.anchor.y;
        const sw = this.width/this.scale.x;
        const sh = this.height/this.scale.x;
        const g = this.toLocal({x, y}, undefined, undefined, true)
        const px = Math.round(g.x+ox/this.scale.x)
        const py = Math.round(g.y+oy/this.scale.y)
        const pi = Math.round(py*sw+px)*4;
        if (px>=0 && px<=sw && py>=0 && py<=sh) {
            if (this.pixelData) {
                if (pi+3<this.pixelData.length) {
                    return this.pixelData[pi+3] > 0
                }
            } else {
                return true
            }
        }
        return false
    }

    /**
     * Sets the sprite scale
     * @param value scale value from ```0``` to ```1``` 
     */
    setScale(value: number) {
        this.scaling.targetScale = value;
        this.scale.set(value, value)
    }

    set zIndex(value: number) {
        super.zIndex = value;
        TouchableObjects.setSortDirty()
    }

    get zIndex(): number {
        return super.zIndex
    }
}

/**
 * The helper object that holds a first touch id and sprite offset point
 * @class
 */
export class DragTouch {
    identifier: number = 0
    isStarted: boolean = false
    pivotPoint: [number, number] = [0, 0]
}

/**
 * The helper object that holds a second touch id and sprite offset point.
 * Also object contains initial scale and rotation.
 * @class
 */
export class ScaleTouch {
    identifier: number = 0
    isStarted: boolean = false
    fromDistance: number = 0
    fromScale: number = 0
    targetScale: number = 0
    minScale: number = 0
    fromRotation: number = 0
}