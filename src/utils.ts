export interface PromisePending<T> extends PromiseLike<T> {
    promise: Promise<T>
    fulfill: (value: T | PromiseLike<T>)=>void
}

export function defer<T>(): PromisePending<T> {
    let fulfill = (value: T | PromiseLike<T>)=>{ }
    let promise = new Promise<T>(resolve=>{ fulfill = resolve })
    let dfd: PromisePending<T> = {
        promise,
        fulfill,
        then: promise.then.bind(promise)
    }
    return dfd
}

export const cookie = {
    set(name: string, value: string, days: number) {
        let expires = '';
        if (days) {
            let date = new Date()
            date.setTime(date.getTime() + (days*24*60*60*1000))
            expires = `expires=${date.toUTCString()}`;
        }
        document.cookie = `${name}=${value ?? ''}; ${expires} path=/`;
    },

    get(name: string): string | null {
        let nameEQ = name + '=';
        let ca = document.cookie.split(';')
        for(let i=0; i<ca.length; i++) {
            let c = ca[i];
            while (c.charAt(0)==' ') c = c.substring(1, c.length)
            if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length, c.length)
        }
        return null
    },
    
    remove(name: string) {   
        document.cookie = name +'=; Path=/; Expires=Thu, 01 Jan 1970 00:00:01 GMT;';
    }
}

export function makeId(length: number = 10): string {
	const possible = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
	let text = '';
	for (let i = 0; i < length; i++) {
		text += possible.charAt(Math.floor(Math.random() * possible.length));
	}
	return text;
}